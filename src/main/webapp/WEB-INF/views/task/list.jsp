<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>List manager</title>
</head>
<body style="padding: 0px 30px">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <span class="navbar-brand mb-0 h1">Task manager</span>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <a class="nav-item nav-link" href="/projects">Project</a>
            <a class="nav-item nav-link" href="/tasks">Task</a>
            <a class="nav-item nav-link" href="#">User</a>
        </ul>
    </div>
</nav>

<table class="table table-bordered table-striped table-dark">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Description</th>
        <th scope="col">Project</th>
        <th scope="col">Status</th>
        <th scope="col">VIEW</th>
        <th scope="col">EDIT</th>
        <th scope="col">REMOVE</th>
    </tr>
    </thead>
    <tbody>
    <c:set var="index" value="0" scope="page"/>
    <c:forEach var="taskDTO" items="${taskDTOList}">
        <c:set var="index" value="${index + 1}" scope="page"/>
        <tr>
            <th scope="row">${index}</th>
            <td>${taskDTO.name}</td>
            <td>${taskDTO.description}</td>
            <c:forEach var="projectDTO" items="${projectDTOList}">
                <c:if test="${projectDTO.id == taskDTO.projectId}">
                    <td>${projectDTO.name}</td>
                </c:if>
                <c:if test="${projectDTO.id != taskDTO.projectId}">
                    <td></td>
                </c:if>
            </c:forEach>
            <td>${taskDTO.readinessStatus}</td>
            <td><a href="/task/view/${taskDTO.id}">VIEW</a></td>
            <td><a href="/task/edit/${taskDTO.id}">EDIT</a></td>
            <td><a href="/task/remove/${taskDTO.id}">REMOVE</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<table>
    <tr>
        <td>
            <form action="/task/add" method="get">
                <input class="btn btn-outline-secondary" type="submit" value="Create">
            </form>
        </td>
        <td>
            <form action="/task/removeAll" method="get">
                <input class="btn btn-outline-secondary" type="submit" value="Clear">
            </form>
        </td>
    </tr>
</table>
</body>
</html>